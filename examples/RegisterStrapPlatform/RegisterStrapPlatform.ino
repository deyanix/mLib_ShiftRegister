#include <ShiftRegister.h>

ShiftRegister reg(6, 8, 10);

void setup() {
  reg.begin();
  reg.setState(HIGH_MINUS);
  reg.setAutorefresh(true);
  reg.setBitOrder(MSBFIRST);
}

void loop() {
  for (int i = 0; i < 6; i++) {
    reg.beginTransmission();
    reg.bitWrite((1 << i) | (1 << (i+2)));
    reg.endTransmission();
    delay(100);
  }
}