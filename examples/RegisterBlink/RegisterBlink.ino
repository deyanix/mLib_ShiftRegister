#include <ShiftRegister.h>

ShiftRegister reg(6, 8, 10);

void setup() {
  reg.begin();
  reg.setState(HIGH_MINUS);
  reg.setAutorefresh(true);
  reg.setBitOrder(MSBFIRST);
}

void loop() {
  reg.pinWrite(0, LOW);
  delay(1000);
  reg.pinWrite(0, HIGH);
  delay(1000);
}
