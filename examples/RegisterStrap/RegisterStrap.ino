#include <ShiftRegister.h>

ShiftRegister reg(6, 8, 10);

void setup() {
  reg.begin();
  reg.setState(HIGH_MINUS);
  reg.setAutorefresh(true);
  reg.setBitOrder(MSBFIRST);
}

int tmp = 0;
void loop() {
  reg.beginTransmission();
  reg.bitSend(tmp = !tmp);
  reg.endTransmission();
  
  delay(100);
}