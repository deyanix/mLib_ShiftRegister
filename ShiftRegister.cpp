#include "ShiftRegister.h"


ShiftRegister::ShiftRegister(int dataPin, int latchPin, int clockPin, int pinCount):dataPin(dataPin),latchPin(latchPin),clockPin(clockPin),pinCount(pinCount) {}

ShiftRegister::ShiftRegister(int dataPin, int latchPin, int clockPin):dataPin(dataPin),latchPin(latchPin),clockPin(clockPin),pinCount(8) {}

void ShiftRegister::begin() {
	pinMode(dataPin, OUTPUT);
    pinMode(latchPin, OUTPUT);
    pinMode(clockPin, OUTPUT);

    reset();
}

void ShiftRegister::reset() {
	value = 0;
	pinRefresh();
}

void ShiftRegister::setType(int type) {
	if (type != HIGH && type != LOW)
		return;
	
	this->type = type;
}

int ShiftRegister::getType() {
	return type;
}

void ShiftRegister::setBitOrder(int bitOrder) {
	if (bitOrder != MSBFIRST && bitOrder != LSBFIRST)
		return;
	this->bitOrder = bitOrder;
}

int ShiftRegister::getBitOrder() {
	return bitOrder;
}

void ShiftRegister::setAutorefresh(bool autorefresh) {
	this->autorefresh = autorefresh;
}

bool ShiftRegister::isAutorefresh() {
	return autorefresh;
}

void ShiftRegister::beginTransmission() {
	digitalWrite(latchPin, LOW);
}

void ShiftRegister::endTransmission() {
	digitalWrite(latchPin, HIGH);
}

void ShiftRegister::bitSend(int data) {
	if (type == LOW)
		data = !data;
	digitalWrite(dataPin, !!data);
	digitalWrite(clockPin, HIGH);
	digitalWrite(clockPin, LOW);
}

void ShiftRegister::bitSerialSend(int data) {
	for (int i = 0; i < pinCount; i++) {
		if (bitOrder == LSBFIRST)
			bitSend(data & (1 << i));
		else
			bitSend(data & (1 << (pinCount - i - 1)));
	}
}

void ShiftRegister::pinRefresh() {
	beginTransmission();
	bitSerialSend(value);
	endTransmission();
	//Serial.print("Rejestr ma wartosc ");
	//Serial.println(value, BIN);
}

void ShiftRegister::pinWrite(int pin, int state) {
	if (state != HIGH && state != LOW) 
		return;
	if (pin < 0 || pin >= pinCount)
		return;

	int bitmask = (1 << pin);
	if (state == HIGH) {
		value |= bitmask;
	} else {
		value ^= bitmask;
	}

	if (autorefresh) 
		pinRefresh();
}