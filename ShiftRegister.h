#ifndef SHIFTREGISTER_H
#define SHIFTREGISTER_H
#include "Arduino.h"

class ShiftRegister {
public: 
	ShiftRegister(int, int, int, int);
	ShiftRegister(int, int, int);

	void reset();
	void begin();
	void setType(int);
	int getType();
	void setBitOrder(int);
	int getBitOrder();
	void setAutorefresh(bool);
	bool isAutorefresh();

	void beginTransmission();
	void endTransmission();
	void bitSend(int);
	void bitSerialSend(int);

	void pinRefresh();
	void pinWrite(int, int);
private:
	const int dataPin, clockPin, latchPin, pinCount;

	bool autorefresh = true;
	int type = HIGH, bitOrder = MSBFIRST;
	int value = 0;
};

#endif